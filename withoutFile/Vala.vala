using Gtk;

public class StyleApp1 : Gtk.Window
{
    public StyleApp1() 
    {

        this.title = "Style app example";
        this.set_border_width (10);
        this.set_position (Gtk.WindowPosition.CENTER);

        this.set_default_size (350, 200);
        this.destroy.connect (Gtk.main_quit);

        var screen = this.get_screen ();
        var css_provider = new Gtk.CssProvider();

        string path = "styleapp1.css";

        // test if the css file exist
        if (FileUtils.test (path, FileTest.EXISTS))
        {
            try {
                css_provider.load_from_data(".my_class { font-size: 17px; color: red }");
                Gtk.StyleContext.add_provider_for_screen(screen, css_provider, Gtk.STYLE_PROVIDER_PRIORITY_USER);
            } catch (Error e) {
                error ("Cannot load CSS stylesheet: %s", e.message);
            }
        }

        var box = new Gtk.Box (Gtk.Orientation.VERTICAL, 10);
        this.add (box);

        var label = new Gtk.Label ("Thank you");
        box.add (label);

        var label2 = new Gtk.Label ("Stackoverflow");
        //
        label2.get_style_context().add_class("my_class");
        box.add (label2);
    }
}

static int main(string[] args) {
    Gtk.init(ref args);

    StyleApp1 win = new StyleApp1();
    win.show_all();

    Gtk.main();
    return 0;
}